package com.rrgomide.loteria.forms;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PrincipalFrame extends JFrame {

  private JLabel labelPrincipal;
  private JTextField numero1;
  private JTextField numero2;
  private JTextField numero3;
  private JTextField numero4;
  private JTextField numero5;
  private JTextField numero6;
  private JButton btnSortear;
  private List<Integer> numeros = new ArrayList<Integer>();
  private JTextField[] numerosSorteados = new JTextField[6];
  
  public PrincipalFrame() {
    
    this.setSize(640, 480);
    this.setLocationRelativeTo(null);
    this.setLayout(new FlowLayout());
    this.setTitle("Loteria");
    
    numero1 = new JTextField();
    numero1.setFont(new Font("Consolas", Font.PLAIN, 78));
    numero1.setText("00"); 
    numerosSorteados[0] = numero1;
    
    numero2 = new JTextField(2);
    numero2.setFont(new Font("Consolas", Font.PLAIN, 78));
    numero2.setText("00");
    numerosSorteados[1] = numero2;
    
    numero3 = new JTextField(2);
    numero3.setFont(new Font("Consolas", Font.PLAIN, 78));
    numero3.setText("00");
    numerosSorteados[2] = numero3;
    
    numero4 = new JTextField(2);
    numero4.setFont(new Font("Consolas", Font.PLAIN, 78));
    numero4.setText("00");
    numerosSorteados[3] = numero4;
    
    numero5 = new JTextField(2);
    numero5.setFont(new Font("Consolas", Font.PLAIN, 78));
    numero5.setText("00");
    numerosSorteados[4] = numero5;
    
    numero6 = new JTextField(2);
    numero6.setFont(new Font("Consolas", Font.PLAIN, 78));
    numero6.setText("00");
    numerosSorteados[5] = numero6;
    
    labelPrincipal = new JLabel("Sorteie os n�meros da MegaSena aqui!");
    labelPrincipal.setFont(new Font("Consolas", Font.BOLD, 28));
    
    btnSortear = new JButton("Sortear!"); 
    btnSortear.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        
        int numeroSorteado;
        
        for(int i = 0; i < 6; i++) {
          
          boolean inseriuNumero = false;
          
          do {
            
            numeroSorteado = Math.min(60, (Math.max(1, (int)Math.ceil(Math.random() * 100.0 - 40.0)) + 1));
            
            if (!numeros.contains(numeroSorteado) ) {              
              inseriuNumero = true;
              numeros.add(numeroSorteado);              
            } 
          } while (!inseriuNumero);         
        }
        
        Collections.sort(numeros);
        
        for(int i = 0; i < 6; i++) {
          String value = String.valueOf(numeros.get(i));
          value = (value.length() == 1) ? "0" + value : value;
          numerosSorteados[i].setText(value);
        }
        
        numeros.clear();
      }
    });
    
    this.add(labelPrincipal); 
    this.add(numero1);
    this.add(numero2);
    this.add(numero3);
    this.add(numero4);
    this.add(numero5);
    this.add(numero6);
    this.add(btnSortear);
  }
}
