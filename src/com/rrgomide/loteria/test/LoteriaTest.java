package com.rrgomide.loteria.test;

import javax.swing.JFrame;

import com.rrgomide.loteria.forms.PrincipalFrame;

public class LoteriaTest {

  public static void main(String[] args) {
   
    PrincipalFrame p = new PrincipalFrame();
    p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    p.setVisible(true);    
  }  
}
